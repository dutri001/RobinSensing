#' Build a MODIS pattern from product name and a list of tiles
#' 
#' @param product charatcer Product name
#' @param tiles vector of characters (e.g. c('h21v12', 'h21v11')) representing the list of tiles to parse
#' @param date An object of class date, can be left empty
#' @param ext character extension (with period)
#' 
#' @author Loic Dutrieux
#' 
#' @return A charater (regex pattern)
#' 
#' @export
#'


MODISpattern <- function(product, tiles = NULL, ext = '.hdf') {
    
    if (is.null(tiles)) {
        tileList <- '.*'
    } else {
        tileList <- paste(tiles, collapse = '|')
    }
    
    
    pattern <- sprintf('^.*%s.*(%s).*\\%s$', product, tileList, ext)
    
    return(pattern)
}